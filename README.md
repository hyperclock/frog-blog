# FrogBlog
Welcome to the FrogBlog! This project is being developed on top of the <a href="https://symfony.com" target="_blank">Symfony Microkernal Framework</a>. Development starts with the latest LTS, which is version 4.4 (at the start of development).

## Features 
* Symfony Framework base
* Multilingual
* Uses Annotations
* Doctrine 
* Backend (Admin Area)
* User Management

### *Planned Features*
* 

## Documentation
Eventually in the [WIKI](../wikis/home)

## License
See [LICENSE](LICENSE) for details.

